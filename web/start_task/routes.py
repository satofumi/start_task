def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('request_uid', '/request_uid')
    config.add_route('task_started', '/task_started')
    config.add_route('data', '/data')
    config.add_route('total', '/total/{year}/{month}/{day}')
