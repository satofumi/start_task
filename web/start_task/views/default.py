from pyramid.view import view_config
from pyramid.response import Response
from sqlalchemy.exc import SQLAlchemyError
import secrets
import json

from .. import models


@view_config(route_name='home', renderer='start_task:templates/home.jinja2')
def home(request):
    try:
        #query = request.dbsession.query(models.MyModel)
        #one = query.filter(models.MyModel.name == 'one').one()
        pass

    except SQLAlchemyError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return {}


@view_config(route_name='request_uid', renderer='json')
def request_uid(request):
    try:
        created_uid = secrets.token_urlsafe(32)

        query = request.dbsession.query(models.User)
        user = query.filter(models.User.uid == created_uid).one_or_none()
        if not user:
            user = models.User(uid=created_uid)
            request.dbsession.add(user)

    except SQLAlchemyError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return {'uid': user.uid}


@view_config(route_name='task_started', renderer='json')
def task_started(request):
    try:
        uid = request.params.get('uid')
        print(uid)

        #query = request.dbsession.query(models.MyModel)
        #one = query.filter(models.MyModel.name == 'one').one()
        pass

    except SQLAlchemyError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return data(request)


@view_config(route_name='data', renderer='json')
def data(request):
    try:
        uid = request.params.get('uid')
        print(uid)

        #query = request.dbsession.query(models.)
        #one = query.filter(models.MyModel.name == 'one').one()
        pass

    except SQLAlchemyError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return {
        'version': '0.0.1',
        'uid': uid,
        'days': 0,
        'weeks': 0,
    }


@view_config(route_name='total', renderer='json')
def total(request):
    try:
        #query = request.dbsession.query(models.)
        #one = query.filter(models.MyModel.name == 'one').one()
        pass

    except SQLAlchemyError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return {
        'date': '2022-04-21',
        'count': 234,
    }



db_err_msg = """\
Pyramid is having a problem using your SQL database.  The problem
might be caused by one of the following things:

1.  You may need to initialize your database tables with `alembic`.
    Check your README.txt for descriptions and try to run it.

2.  Your database server may not be running.  Check that the
    database server referred to by the "sqlalchemy.url" setting in
    your "development.ini" file is running.

After you fix the problem, please restart the Pyramid application to
try it again.
"""
