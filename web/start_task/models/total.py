from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    Date,
)

from .meta import Base
from datetime import datetime


class Total(Base):
    __tablename__ = 'total'
    date = Column(Date, default=datetime.today, primary_key=True)
    count = Column(Integer)
