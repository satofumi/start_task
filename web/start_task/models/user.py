from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    DateTime,
)

from .meta import Base
from datetime import datetime


class User(Base):
    __tablename__ = 'users'
    uid = Column(Text, primary_key=True)
    datetime = Column(DateTime, default=datetime.now)
