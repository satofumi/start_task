from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    DateTime,
)

from .meta import Base
from datetime import datetime


class Record(Base):
    __tablename__ = 'records'
    uid = Column(Text, primary_key=True)
    datetime = Column(DateTime, default=datetime.now, primary_key=True)
    count = Column(Integer)
