from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
)

from .meta import Base


class Count(Base):
    __tablename__ = 'counts'
    uid = Column(Text, primary_key=True)
    days = Column(Integer)
    weeks = Column(Integer)
